"use strict"

const { PersistCodec } = require('codec')

module.exports.to = async function() {
  const output = {}

  output.$script = this.script
  output.$specialSlots = this.items.special.map(o => ({$x: o.x, $y: o.y}))
  output.$scoreSlots = this.items.score.map(o => ({$x: o.x, $y: o.y}))
  output.$badList = this.bads.map(o => ({$id: o.type, $x: o.x, $y: o.y}))
  output.$playerX = this.player.x
  output.$playerY = this.player.y
  output.$skinTiles = this.skins.tiles.v
  if (this.skins.tiles.v != this.skins.tiles.h) {
    output.$skinTiles = output.$skinTiles * 100 + this.skins.tiles.h
  }
  output.$skinBg = this.skins.background
  output.$map = []
  for (const i in this.map) {
    for (const j in this.map[i]) {
      if (output.$map[j] === undefined)
        output.$map[j] = []
      output.$map[j][i] = this.map[i][j]
    }
  }
  return (new PersistCodec).encode(output)
}

module.exports.from = function(encodedLevel) {
  const Level = require('..')
  const output = new Level()
  const decoded = (new PersistCodec).decode(encodedLevel)

  output.script = decoded.$script
  output.items.special = decoded.$specialSlots.map(o => ({x: o.$x, y: o.$y}))
  output.items.score = decoded.$scoreSlots.map(o => ({x: o.$x, y: o.$y}))
  decoded.$badList.forEach(o => output.addBad(o.$id, o.$x, o.$y))
  output.player.x = decoded.$playerX
  output.player.y = decoded.$playerY
  if (decoded.$skinTiles > 100) {
    output.skins.tiles.h = decoded.$skinTiles % 100
    output.skins.tiles.v = parseInt(decoded.$skinTiles / 100)
  } else {
    output.skins.tiles.h = decoded.$skinTiles
    output.skins.tiles.v = decoded.$skinTiles
  }
  output.skins.background = decoded.$skinBg
  for (const i in decoded.$map) {
    for (const j in decoded.$map[i]) {
      output.map[j][i] = decoded.$map[i][j]
    }
  }
  return output
}

module.exports.name = "Hammerfest"
module.exports.short = "HF"
module.exports.fileFormats = ["hf"]
