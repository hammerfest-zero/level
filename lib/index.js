"use strict"

const fs = require('fs')
const path = require('path')

class Level {
  constructor() {
    this.script = ''
    this.items = {
      special: [],
      score: []
    }
    this.bads = []
    this.player = {
      x: 0,
      y: 0
    }
    this.skins = {
      tiles: {
        h: 1,
        v: 1
      },
      background: 1
    }
    this.map = []
    for (let i = 0; i < 25; ++i) {
      const line = []
      for (let j = 0; j < 20; ++j) {
        line.push(0)
      }
      this.map.push(line)
    }
  }

  toJSON() {
    return {
      script: this.script,
      items: this.items,
      bads: this.bads,
      player: this.player,
      skins: this.skins,
      map: this.map
    }
  }

  addBad(type, x, y) {
    this.bads.push({
      type: type,
      x: x,
      y: y
    })
  }
}

const formats = {}

function requireOne(file) {
  const format = require(file)

  if (format.short && format.to) {
    Level.prototype["to" + format.short] = format.to
  }
  if (format.short && format.from) {
    Level["from" + format.short] = format.from
  }
  formats[format.name] = []
  ;["name", "short", "fileFormats"].forEach((m) => formats[format.name][m] = format[m])
}

fs.readdirSync(__dirname).filter((f) => f !== 'index.js')
  .forEach((f) => requireOne(path.join(__dirname, f)))

module.exports = Level
module.exports.formats = formats
