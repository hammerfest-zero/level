"use strict"

const fs = require('fs').promises
const path = require('path')
const { createSVGWindow } = require('svgdom')
const { SVG, registerWindow } = require('@svgdotjs/svg.js')

async function setBackground(canvas, level) {
  const bgFile = await fs.readFile(path.join(__dirname, '../assets/images/backgrounds/', level.skins.background + '.svg'), 'utf8')
  const bgDef = SVG(bgFile).id('bg')
  canvas.defs().svg(bgDef.svg())

  canvas.use('bg').height(500).width(420)
}

function writeHPlatform(canvas, i, j, length) {
  canvas.use('hpf-body')
    .move((j - length) * 20, i * 20)
    .width(length * 20 - 5)
  canvas.use('hpf-end')
    .move(j * 20 - 5, i * 20)
}

function writeVPlatform(canvas, i, j, length) {
  if (length > 1) {
    canvas.use('vpf-body')
      .width(length * 20 - 5)
      .attr('transform', 'matrix(0, 1, 1, 0, ' + j * 20 + ', ' + (i - length) * 20 + ')')
    canvas.use('vpf-end')
      .attr('transform', 'matrix(0, 1, 1, 0, ' + j * 20 + ', ' + (i * 20 - 5) + ')')
  } else if (length === 1) {
    writeHPlatform(canvas, i - 1, parseInt(j) + 1, length)
  }
}

async function setPlatforms(canvas, level) {
  const hpfBodyFile = await fs.readFile(path.join(__dirname, '../assets/images/platforms/bodies', level.skins.tiles.h + '.svg'), 'utf8')
  const hpfEndFile = await fs.readFile(path.join(__dirname, '../assets/images/platforms/ends', level.skins.tiles.h + '.svg'), 'utf8')
  const vpfBodyFile = await fs.readFile(path.join(__dirname, '../assets/images/platforms/bodies', level.skins.tiles.v + '.svg'), 'utf8')
  const vpfEndFile = await fs.readFile(path.join(__dirname, '../assets/images/platforms/ends', level.skins.tiles.v + '.svg'), 'utf8')
  const hpfBodyDef = SVG(hpfBodyFile).id('hpf-body')
  const hpfEndDef = SVG(hpfEndFile).id('hpf-end')
  const vpfBodyDef = SVG(vpfBodyFile).id('vpf-body')
  const vpfEndDef = SVG(vpfEndFile).id('vpf-end')
  canvas.defs().svg(hpfBodyDef.svg())
  canvas.defs().svg(hpfEndDef.svg())
  canvas.defs().svg(vpfBodyDef.svg())
  canvas.defs().svg(vpfEndDef.svg())

  const map = JSON.parse(JSON.stringify(level.map)) // deep copy
  for (const i in map) {
    let lineLength = 0
    for (const j in map[i]) {
      if (map[i][j] === 1) {
        lineLength += 1
        if (lineLength > 1) {
          map[i][j - 1 < 0 ? 0 : j - 1] = -100
          map[i][j] = -100
        }
      } else {
        if (lineLength > 1)
          writeHPlatform(canvas, i, j, lineLength)
        lineLength = 0
      }
    }
    if (lineLength > 1)
      writeHPlatform(canvas, i, map[i].length, lineLength)
  }
  for (const j in map[0]) {
    let lineLength = 0
    for (const i in map) {
      if (map[i][j] === 1) {
        lineLength += 1
        map[i][j] = -100
      } else {
        writeVPlatform(canvas, i, j, lineLength)
        lineLength = 0
      }
    }
    writeVPlatform(canvas, map.length, j, lineLength)
  }
}

module.exports.to = async function() {
  const window = createSVGWindow()
  const document = window.document
  registerWindow(window, document)
  const canvas = SVG(document.documentElement)

  await setBackground(canvas, this)
  await setPlatforms(canvas, this)

  return canvas.svg()
}

module.exports.name = "SVG"
module.exports.short = "SVG"
module.exports.fileFormats = ["svg"]
