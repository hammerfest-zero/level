"use strict"

const hfFormat = require('./hf')

module.exports.to = hfFormat.to
module.exports.from = hfFormat.from

module.exports.name = "Hammerfest Zero"
module.exports.short = "HF0"
module.exports.fileFormats = ["hf0"]
