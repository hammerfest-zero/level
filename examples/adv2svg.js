#!/usr/bin/env node

const fs = require("fs").promises
const Level = require("..")

async function exportLevels() {
  const levels = await fs.readFile("adventure.lvl", "utf-8")
  splits = levels.split(":")
  for (let i in splits) {
    const l = await Level.fromHF(splits[i])
    await fs.writeFile(i + ".svg", await l.toSVG())
  }
}

exportLevels()
