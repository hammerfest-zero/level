# [WIP] Level Library

The Level library is a libary which goal is to convert levels from one format to another, including Hammerfest-Zero format, Hammerfest format, moulins' editor format and images (output only)

```sh
npm install git+http://gitlab.com/hammerfest-zero/level.git
```

## Usage

```js
const Level = require('level')

async function doStuff() {
  const level = Level.fromHF("SVjoEpLP4DobnYtNJ38STpzV6C4SuK8dCuK8HFoYtNWY49ohzt2B-NokqWyxZeKXyCKZtZ6ry5jPYdreIziASaqYywPI5dldNZuK569o6PP2zEzxs8K569o6fNcMjjW2yFoxLzaMjjW2yFoxLyaLjjTW8iGaaaaGaaaaaaaaaeaaaaaaaaaaaaadqaaaaqaaaaaaaaacaaaaaaaaaaaaabOaaaaiecbaGqiaabaaaaaaaaaaaaaa0aaaa5dMoQoi6G5JQGaaeaaaaaaaaaaaaadqaaadKoy6O4JQdMoQaaaqaaaaaaaaaaaaanaaaabaGqiecbaaaiaaaaaaaaaaaaagGaaaaGaaaaaaaaaeaaaaaaaaaaaaadqaaaaqaaaaaaaaacaaaaaaaaaaaaabOaaaaiaaaaaaaaabaaaaaaaaaaaaaa0aaaaeaaaaaaaaaaGaaaaaaaaaaaaaAaaaacaaaaaaaaaaqaaaaaaaaaaaaanaaaabaaaaaaaaaaiaaaaaaaaaaaaagGaaaaGaaaaaaaaaeaaaaaaaaaaaaadqaaaaqaaaaaaaaacaaaaaaaaaaaaabOaaaaiecbaGqiaabaaaiecaaaaaaaa0aaaa5JKoOoQoy4JMaaaqaaaaaGaaaaaaanaaaaoy5dQdQJMoi5GaaeaaaaaiaaaaaaadqaaaaqiecbaGqaacaaaaaeaaaaaaabOaaaaiaaaaaaaaaaaaaaacaaaaaaaa0aaaaeaaaaaaaaaaaaaaabaaaaaaaaFG") // level 11.3 from original game
  console.log(await level.toSVG())
}

doStuff()
```

## Implementation status

| Format | Import | Export
|---|---|---|
| Hammerfest | ✔️ | ✔️ |
| Hammerfest Zero | ✔️ | ✔️ |
| Moulins | planned | WIP¹ |
| Eternalfest | ❌² | ❌² |
| SVG | ❌ | partial³ |

¹ See [Théo Chapelle's fork](https://gitlab.com/FireKing54/level)  
² Even if this format is not officially supported, using the original format *might* work in some cases  
³ At the moment, only the background and the platforms are displayed
